const EnumButton = [
  { id: '1', color: 'purple', name: 'PURPLE' },
  { id: '2', color: 'blue', name: 'BLUE' },
  { id: '3', color: 'pink', name: 'PINK' },
  { id: '4', color: 'orange', name: 'ORANGE' },
  { id: '5', color: 'green', name: 'GREEN' },
  { id: '6', color: 'yellow', name: 'YELLOW' },
  { id: '7', color: 'grey', name: 'GREY' },
  { id: '8', color: 'brown', name: 'BROWN' },
  { id: '9', color: 'purple', name: 'PURPLE' },
  { id: '10', color: 'green', name: 'GREEN' },
  { id: '11', color: 'red', name: 'RED' },
  { id: '12', color: 'green', name: 'GREEN' },
  { id: '13', color: 'red', name: 'RED' },
  { id: '14', color: 'grey', name: 'GREY' },
  { id: '15', color: 'orange', name: 'ORANGE' },
  { id: '16', color: 'blue', name: 'BLUE' },
];

export default EnumButton;
