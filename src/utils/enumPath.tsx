enum PathRoutes {
  Home = '/',
  Colors = '/colors',
  TicTacToe = '/tictactoe',
}

export default PathRoutes;
