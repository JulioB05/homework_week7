const combos = [
  { num1: 0, num2: 1, num3: 2 },
  { num1: 3, num2: 4, num3: 5 },
  { num1: 6, num2: 7, num3: 8 },
  { num1: 0, num2: 3, num3: 6 },
  { num1: 1, num2: 4, num3: 7 },
  { num1: 2, num2: 5, num3: 8 },
  { num1: 0, num2: 4, num3: 8 },
  { num1: 2, num2: 4, num3: 6 },
];

const sizeTicTac = [
  { square: 1, num: '0' },
  { square: 2, num: '1' },
  { square: 3, num: '2' },
  { square: 4, num: '3' },
  { square: 5, num: '4' },
  { square: 6, num: '5' },
  { square: 7, num: '6' },
  { square: 8, num: '7' },
  { square: 9, num: '8' },
];
export { combos, sizeTicTac };
