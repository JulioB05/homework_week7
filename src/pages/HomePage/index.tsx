import React from 'react';
import { Link } from 'react-router-dom';
import './style.scss';
import PathRoutes from '../../utils/enumPath';
import colors from '../../images/colors.jpg';
import tictactoe from '../../images/tictactoe.png';

const Home = () => {
  return (
    <div className="body">
      <div>
        <div className="title">
          <h1>HOMEWORK WEEK 7</h1>
        </div>
        <div className="box">
          <Link to={PathRoutes.Colors} className="link">
            <img src={colors} alt="" />
            <h2>GAME TIME MACHINE COLORS</h2>
          </Link>
          <span className="loader"></span>
          <Link to={PathRoutes.TicTacToe} className="link">
            <img src={tictactoe} alt="" />
            <h2>GAME TIC TAC TOE</h2>
          </Link>
        </div>
      </div>
    </div>
  );
};
export default Home;
