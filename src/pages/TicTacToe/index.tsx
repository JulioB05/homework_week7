import React from 'react';
import TicTacToeButtons from '../../components/TicTacToeGame.tsx';
import './style.scss';

const TicTacToe = () => {
  return (
    <div className="ticGame">
      <TicTacToeButtons></TicTacToeButtons>
    </div>
  );
};

export default TicTacToe;
