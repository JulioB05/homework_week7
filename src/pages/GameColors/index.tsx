import React from 'react';
import TimeMachineColors from '../../components/TimeMachineColors';
import './style.scss';

const GameColors = () => {
  return (
    <div className="gameColors">
      <TimeMachineColors />
    </div>
  );
};

export default GameColors;
