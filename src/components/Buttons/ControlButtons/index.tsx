import React from 'react';
import './style.scss';

interface ControlButtonsProps {
  stateButtonNext: boolean;
  nextFunction: () => void;
  stateButtonPresent: boolean;
  presentFunction: () => void;
  stateButtonPrevious: boolean;
  previousFunction: () => void;
  nameBtn: string;
}

const ControlButtons = (props: ControlButtonsProps) => {
  const {
    stateButtonNext,
    nextFunction,
    stateButtonPresent,
    presentFunction,
    stateButtonPrevious,
    previousFunction,
    nameBtn,
  } = props;
  return (
    <>
      <button
        className={`btn-next  ${
          stateButtonNext ? 'opacity-btn-control' : 'active-btn-control'
        }`}
        onClick={nextFunction}
        disabled={stateButtonNext}
      >
        NEXT
      </button>
      <button
        className={`btn-present  ${
          stateButtonPresent ? 'opacity-btn-control' : 'active-btn-control'
        }`}
        onClick={presentFunction}
        disabled={stateButtonPresent}
      >
        {nameBtn}
      </button>
      <button
        className={`btn-previous  ${
          stateButtonPrevious ? 'opacity-btn-control' : 'active-btn-control'
        }`}
        onClick={previousFunction}
        disabled={stateButtonPrevious}
      >
        PREVIOUS
      </button>
    </>
  );
};

export default ControlButtons;
