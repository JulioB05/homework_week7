import React, { Dispatch, SetStateAction } from 'react';
import './style.scss';

interface ColorButtonsProps {
  colorTraveling: string;
  travelingState: boolean;
  countColorsClicked: number;
  setcolorSelected: Dispatch<SetStateAction<string>>;
  setCountColorsClicked: Dispatch<SetStateAction<number>>;
  setColorTraveling: Dispatch<SetStateAction<string>>;
  color: string;
  id: string;
  name: string;
}
const ColorButtons = (props: ColorButtonsProps) => {
  const {
    colorTraveling,
    travelingState,
    countColorsClicked,
    setcolorSelected,
    setCountColorsClicked,
    setColorTraveling,
    color,
    id,
    name,
  } = props;

  const sendColorId = (id: string) => {
    setcolorSelected(id);
    setCountColorsClicked(countColorsClicked + 1);
    setColorTraveling(id);
  };

  return (
    <button
      disabled={travelingState}
      onClick={() => sendColorId(id)}
      id={id}
      className={`grid-item-${color} ${
        colorTraveling === id ? ' active' : 'opacity'
      }`}
    >
      {name}
    </button>
  );
};

export default ColorButtons;
