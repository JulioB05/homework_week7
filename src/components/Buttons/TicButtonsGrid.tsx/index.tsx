/* eslint-disable no-empty */
import React from 'react';
import { Dispatch, SetStateAction } from 'react';
import { combos } from '../../../utils/enumTicTacToe';
import './style.scss';

interface TicButtonsProps {
  num: string;
  valueBox: string;
  cells: string[];
  turn: string;
  setSquareSelect: Dispatch<SetStateAction<string>>;
  setTurn: Dispatch<SetStateAction<string>>;
  setCells: Dispatch<SetStateAction<string[]>>;
  setWinner: Dispatch<SetStateAction<string>>;
  winner: string;
  squareTraveling: string;
  traveling: boolean;
  setCountClicked: Dispatch<SetStateAction<number>>;
  countClicked: number;
  setSquareTraveling: Dispatch<SetStateAction<string>>;
}

const TicButtons = (props: TicButtonsProps) => {
  const {
    num,
    valueBox,
    cells,
    turn,
    setSquareSelect,
    setTurn,
    setCells,
    setWinner,
    winner,
    squareTraveling,
    traveling,
    setCountClicked,
    countClicked,
    setSquareTraveling,
  } = props;

  const checkWinner = (squares: string[]) => {
    combos.map((combo) => {
      if (
        squares[combo.num1] === '' ||
        squares[combo.num2] === '' ||
        squares[combo.num3] === ''
      ) {
      } else if (
        squares[combo.num1] === squares[combo.num2] &&
        squares[combo.num2] === squares[combo.num3]
      ) {
        setWinner(squares[combo.num1]);
      }
    });
  };

  const handleClick = (num: string) => {
    if (cells[parseInt(num)] === '') {
      const squares = [...cells];

      if (turn === 'X') {
        squares[parseInt(num)] = 'X';

        setSquareSelect(num);
        setSquareTraveling(num);
        setTurn('O');
      } else {
        squares[parseInt(num)] = 'O';

        setSquareSelect(num);
        setSquareTraveling(num);
        setTurn('X');
      }
      setCells(squares);
      checkWinner(squares);
      setCountClicked(countClicked + 1);
    }
  };

  return (
    <button
      className={`btn-squares ${
        squareTraveling === num ? 'active-square' : 'opacity-square'
      }`}
      disabled={winner !== '' || traveling ? true : false}
      onClick={() => handleClick(num)}
    >
      {valueBox}
    </button>
  );
};

export default TicButtons;
