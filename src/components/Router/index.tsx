import React from 'react';
import { BrowserRouter, Route, Routes } from 'react-router-dom';
import GameColors from '../../pages/GameColors';
import Home from '../../pages/HomePage';
import TicTacToe from '../../pages/TicTacToe';
import PathRoutes from '../../utils/enumPath';

const Router = () => {
  return (
    <BrowserRouter>
      <Routes>
        <Route path={PathRoutes.Home} element={<Home />} />
        <Route path={PathRoutes.Colors} element={<GameColors />} />
        <Route path={PathRoutes.TicTacToe} element={<TicTacToe />} />
      </Routes>
    </BrowserRouter>
  );
};

export default Router;
