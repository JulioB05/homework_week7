import React, { useMemo, useState } from 'react';
import useTimeMachine from '../../hooks/useTimeMachine';
import ColorButtons from '../Buttons/ColorButtons';
import EnumButton from '../../utils/enumButtons';
import PathRoutes from '../../utils/enumPath';
import './style.scss';
import { Link } from 'react-router-dom';
import ControlButtons from '../Buttons/ControlButtons';

const TimeMachineColors = () => {
  const [travelingState, setTravelingState] = useState(false);
  const [countColorsClicked, setCountColorsClicked] = useState(0);
  const [travelingPosition, setTravelingPosition] = useState(0);
  const [stateButtonNext, setstateButtonNext] = useState(true);
  const [stateButtonPresent, setstateButtonPresent] = useState(true);
  const [stateButtonPrevious, setstateButtonPrevious] = useState(true);
  const [colorTraveling, setColorTraveling] = useState('');
  const [colorSelected, setcolorSelected] = useState(''); //agrega al hook
  const { getPreviousStateN } = useTimeMachine(colorSelected);

  useMemo(() => {
    if (countColorsClicked > 0) {
      setstateButtonPrevious(false);
    }

    if (travelingPosition === 0) {
      setstateButtonNext(true);
    }

    if (travelingPosition === countColorsClicked - 1) {
      setstateButtonPrevious(true);
    }
  }, [countColorsClicked, travelingPosition]);

  //--------------------- NEXT --------------------------------------
  const nextFunction = () => {
    const numberNextPosition = travelingPosition - 1;
    if (travelingPosition > 0) {
      setTravelingPosition(numberNextPosition);
      const next = getPreviousStateN(numberNextPosition);
      setColorTraveling(next);
    }
  };
  //--------------------- PRESENT --------------------------------------
  const presentFunction = () => {
    setstateButtonPresent(true);
    setTravelingPosition(0);
    setColorTraveling(colorSelected);
    setTravelingState(false);
  };
  //--------------------- PREVIOUS --------------------------------------
  const previousFunction = () => {
    setTravelingState(true);
    const numberPrevPosition = travelingPosition + 1;
    setstateButtonNext(false);
    setstateButtonPresent(false);
    setTravelingPosition(numberPrevPosition);
    const previous = getPreviousStateN(numberPrevPosition);
    setColorTraveling(previous);
  };

  return (
    <div className="container">
      <div className="title">
        <Link to={PathRoutes.Home} className="link">
          <h1 className="h1-title">TIME MACHINE COLORS - CLICK TO HOME</h1>
        </Link>
      </div>
      <div className="game">
        <div className="grid-container">
          {EnumButton.length
            ? EnumButton?.map((btn) => {
                return (
                  <ColorButtons
                    key={btn.id}
                    colorTraveling={colorTraveling}
                    travelingState={travelingState}
                    countColorsClicked={countColorsClicked}
                    setcolorSelected={setcolorSelected}
                    setCountColorsClicked={setCountColorsClicked}
                    setColorTraveling={setColorTraveling}
                    color={btn.color}
                    id={btn.id}
                    name={btn.name}
                  ></ColorButtons>
                );
              })
            : null}
        </div>

        <div className="buttons-container">
          <ControlButtons
            stateButtonNext={stateButtonNext}
            nextFunction={nextFunction}
            stateButtonPresent={stateButtonPresent}
            presentFunction={presentFunction}
            stateButtonPrevious={stateButtonPrevious}
            previousFunction={previousFunction}
            nameBtn="RESUME"
          ></ControlButtons>
        </div>
      </div>
    </div>
  );
};

export default TimeMachineColors;
