import React, { useMemo, useState } from 'react';
import useTimeMachine from '../../hooks/useTimeMachine';
import ControlButtons from '../Buttons/ControlButtons';
import TicButtons from '../Buttons/TicButtonsGrid.tsx';
import PathRoutes from '../../utils/enumPath';
import { sizeTicTac } from '../../utils/enumTicTacToe';
import './style.scss';
import { Link } from 'react-router-dom';

const TicTacToeButtons = () => {
  //-----------------State buttons----------------------------
  const [stateButtonNext, setstateButtonNext] = useState(true);
  const [stateButtonPresent, setstateButtonPresent] = useState(true);
  const [stateButtonPrevious, setstateButtonPrevious] = useState(true);

  //-----------------state traveling---------------------------
  const [countClicked, setCountClicked] = useState(0);
  const [squareTraveling, setSquareTraveling] = useState('');
  const [travelingPosition, setTravelingPosition] = useState(0);
  const [traveling, setTraveling] = useState(false);
  //------------------state for game---------------------------
  const [turn, setTurn] = useState('X');
  const [squareSelect, setSquareSelect] = useState('');
  const [cells, setCells] = useState(Array(9).fill(''));
  const [winner, setWinner] = useState('');
  const { getPreviousStateN, reset } = useTimeMachine(squareSelect);
  //----------------button conditional controls----------------
  useMemo(() => {
    if (winner !== '') {
      setstateButtonPresent(false);
    }
    if (travelingPosition === 0) {
      setstateButtonNext(true);
    }
    if (travelingPosition === countClicked - 1) {
      setstateButtonPrevious(true);
    }
  }, [countClicked, travelingPosition, winner]);
  //---------------RESTART----------------------------------
  const restartFunction = () => {
    reset();
    setSquareSelect('');
    setCells(Array(9).fill(''));
    setWinner('');
    setTravelingPosition(0);
    setTraveling(false);
    setCountClicked(0);
    setstateButtonPresent(true);
    setstateButtonPrevious(true);
    setSquareTraveling('');
  };
  //--------------------- NEXT --------------------------------------
  const nextFunction = () => {
    if (travelingPosition > 0) {
      setTravelingPosition(travelingPosition - 1);
      const next = getPreviousStateN(travelingPosition - 1);
      setSquareTraveling(next);
    }
  };
  //--------------------- REPLAY --------------------------------------
  const replayFunction = () => {
    let count = countClicked - 1;

    setCells(Array(9).fill(''));

    const squares = Array(9).fill('');
    const interval = setInterval(() => {
      if (count >= 0) {
        const position = getPreviousStateN(count);
        const value = cells[parseInt(position)];

        squares[parseInt(position)] = value;
        setSquareTraveling(position);
        count -= 1;
        setCells(squares);
      } else {
        setstateButtonPrevious(false);
        clearInterval(interval);
      }
    }, 500);
  };

  //--------------------- PREVIOUS --------------------------------------
  const previousFunction = () => {
    setTraveling(true);
    setstateButtonNext(false);
    setTravelingPosition(travelingPosition + 1);
    const previous = getPreviousStateN(travelingPosition + 1);
    setSquareTraveling(previous);
  };

  return (
    <div>
      <div className="title">
        <Link to={PathRoutes.Home} className="link">
          <h1 className="h1-title"> GAME TIC TAC TOE - CLICK TO HOME</h1>
        </Link>
      </div>
      <div className="grid-box">
        <div className="container-grid">
          {sizeTicTac.length
            ? sizeTicTac.map((box) => {
                return (
                  <TicButtons
                    key={box.square}
                    num={box.num}
                    valueBox={cells[parseInt(box.num)]}
                    cells={cells}
                    turn={turn}
                    setSquareSelect={setSquareSelect}
                    setTurn={setTurn}
                    setCells={setCells}
                    setWinner={setWinner}
                    winner={winner}
                    squareTraveling={squareTraveling}
                    traveling={traveling}
                    setCountClicked={setCountClicked}
                    countClicked={countClicked}
                    setSquareTraveling={setSquareTraveling}
                  ></TicButtons>
                );
              })
            : null}
        </div>

        <div className="btn-container">
          <ControlButtons
            stateButtonNext={stateButtonNext}
            nextFunction={nextFunction}
            stateButtonPresent={stateButtonPresent}
            presentFunction={replayFunction}
            nameBtn={winner ? 'REPLAY' : 'RESUME'}
            stateButtonPrevious={stateButtonPrevious}
            previousFunction={previousFunction}
          ></ControlButtons>
          {winner && (
            <>
              <p className="winner-title">{winner} is the winner!</p>
            </>
          )}

          <div className="next-move">
            <h3 className="title">NEXT TO MOVE</h3>
            <div className="box">{turn}</div>
          </div>

          <button className="btn-control" onClick={restartFunction}>
            RESTART
          </button>
        </div>
      </div>
    </div>
  );
};

export default TicTacToeButtons;
