import { useState, useRef, useEffect } from 'react';

function useTimeMachine<T>(count: T) {
  const stateEnqueued = useRef(count);
  const [previosStateTimeN, setPreviosStateTimeN] = useState<T | null>();
  const [records, setRecords] = useState<T[]>([]);

  useEffect(() => {
    stateEnqueued.current = count;
    setRecords([stateEnqueued.current, ...records]);
  }, [count]);

  const getPreviousStateN = (n: number) => {
    let prev = 0;
    let numberN = 0;
    if (typeof n === 'number') {
      prev = n + 1;
      numberN = n;
    }
    setPreviosStateTimeN(records[prev]);
    return records[numberN];
  };

  function reset() {
    setPreviosStateTimeN(null);
    setRecords([]);
  }

  return { records, previosStateTimeN, getPreviousStateN, reset };
}

export default useTimeMachine;
