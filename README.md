This application contains an implementation of vercel in the following link:

https://homework-week7.vercel.app/

In this application you can find two games:

- Time Machine Colors / Which consists of a grid of colors, where you can travel in time to see the sequence you previously selected and/or continue selecting

- Tic Tac Toe / Classic game where you can have fun and find out who was the winner. You can also reload the game and travel through time step by step
